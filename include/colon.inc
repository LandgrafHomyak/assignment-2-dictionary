struc dict_entry
    dict_entry$key:     resq 1
    dict_entry$value:   resq 1
    dict_entry$prev:    resq 1
endstruc

%macro colon 4
    %1$key: db %2, 0
    %1$value: db %3, 0
    %1: istruc dict_entry
        at dict_entry$key,   dq %1$key
        at dict_entry$value, dq %1$value
        at dict_entry$prev,  dq %4
    iend
%endmacro