extern _run_app
    ; Main function without dictionary
    ; Arguments:
    ;   rdi - pointer to first dict entry
    ; Returns:
    ;   noreturn
    ; Locals:
    ;   r15 - address of matched entry
