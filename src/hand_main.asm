%include "colon.inc"
%include "base_main.inc"

section .rdata
colon e1, "k1", "v1", 0
colon e2, "k2", "v2", e1
colon e3, "k3", "v3", e2
colon e4, "k4", "v4", e3
colon e5, "k5", "v5", e4

section .text

    global _start
_start:
    mov rdi, e5
    call _run_app
