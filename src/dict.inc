extern find_match
    ; Fills global buffer with data from stdin (stream 0)
    ; Arguments:
    ;   rdi - pointer to first entry in dict
    ;   rsi - pointer to string to find in
    ; Returns:
    ;   void
    ; Locals:
    ;   r14 - saved pointer to current position in string
    ;   r15 - pointer to first entry in dictionary