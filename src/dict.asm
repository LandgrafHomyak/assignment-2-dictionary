%include "lib.inc"
%include "colon.inc"

section .text

    ; Finds prefix of specified string in dict
    ; Arguments:
    ;   rdi - pointer to first entry in dict
    ;   rsi - pointer to string
    ; Returns:
    ;   void
    ; Locals:
    ;   r14 - saved pointer to string
    ;   r15 - pointer to current dictionary entry
iter_dict_prefixes:
    push r15
    push r14
    mov r15, rdi
    mov r14, rsi

iter_dict_prefixes$L1:
    cmp r15, 0
    je iter_dict_prefixes$L2
    mov rdi, r14
    mov rsi, [r15 + dict_entry$key]
    call starts_with
    cmp rax, 0
    jne iter_dict_prefixes$L3
    mov r15, [r15 + dict_entry$prev]
    jmp iter_dict_prefixes$L1

iter_dict_prefixes$L2:
    pop r14
    pop r15
    mov rax, 0
    ret

iter_dict_prefixes$L3:
    mov rax, r15
    pop r14
    pop r15
    ret


    ; Fills global buffer with data from stdin (stream 0)
    ; Arguments:
    ;   rdi - pointer to first entry in dict
    ;   rsi - pointer to string to find in
    ; Returns:
    ;   void
    ; Locals:
    ;   r14 - saved pointer to current position in string
    ;   r15 - pointer to first entry in dictionary
    global find_match;
find_match:
    push r15
    push r14
    mov r15, rdi
    mov r14, rsi

find_match$L1:
    cmp BYTE [r14], 0
    je find_match$L2
    mov rdi, r15
    mov rsi, r14
    call iter_dict_prefixes
    cmp rax, 0
    jne find_match$L3
    inc r14
    jmp find_match$L1

find_match$L2:
    pop r14
    pop r15
    mov rax, 0
    ret

find_match$L3:
    pop r14
    pop r15
    ret
