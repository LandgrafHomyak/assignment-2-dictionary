%include "lib.inc"
%include "colon.inc"
%include "dict.inc"

section .bss
in_buffer: resb 256
in_buffer_length: equ $-in_buffer
resb 1

section .rdata
not_found: db "Matches not found", 10, 0
found: db "Found:", 10, 0
found_key: db "    Key: ", 0
found_value: db "    Value: ", 0


section .text

    ; Fills global buffer with data from stdin (stream 0)
    ; Arguments:
    ; Returns:
    ;   void
    ; Locals:
    ;   r14 - pointer to buffer
    ;   r15 - uninitialized chars count
_fill_buffer:
    mov BYTE [in_buffer + in_buffer_length], 0
    push r15
    push r14
    mov r15, in_buffer_length
    mov r14, in_buffer

_fill_buffer$L1:
    cmp r15, 0
    je _fill_buffer$L2
    call read_char
    mov BYTE [r14], al
    inc r14
    dec r15
    jmp _fill_buffer$L1

_fill_buffer$L2:
    pop r14
    pop r15
    ret



    ; Main function without dictionary
    ; Arguments:
    ;   rdi - pointer to first dict entry
    ; Returns:
    ;   noreturn
    ; Locals:
    ;   r15 - address of matched entry
    global _run_app
_run_app:
    push rdi
    call _fill_buffer
    pop rdi

    mov rsi, in_buffer
    call find_match

    cmp rax, 0
    je _run_app$L1

    mov rdi, [rax + dict_entry$value]
    call print_string

_run_app$L1:
    call print_newline

    mov rdi, 0
    call exit
    ret