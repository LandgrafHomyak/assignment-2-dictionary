#!fuck python

import sys
import subprocess

if len(sys.argv) < 4:
    print("Wrong usage")
    exit(10)

with open(sys.argv[1], "rt") as f_input:
    input = f_input.read()

with open(sys.argv[2], "rt") as f_expected:
    expected = f_expected.read()

p_actual = subprocess.Popen(sys.argv[4:], stdin=subprocess.PIPE, stdout=subprocess.PIPE)
try:
    (actual, _) = p_actual.communicate(input.encode(), 10)
except subprocess.TimeoutExpired:
    p_actual.kill()
    print("timout exceed")
    exit(12)

with open(sys.argv[3], "wt") as f_actual:
    f_actual.write(actual.decode())

if p_actual.returncode != 0:
    exit(p_actual.returncode)

if expected == actual.decode():
    print("Ok")
    exit(0)
else:
    print("Not ok")
    print("expected len", len(expected))
    print("actual len", len(actual))
    exit(11)
